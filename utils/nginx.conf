# Configuración base de nginx para uso en droplets de digitalocean
# Tiene ejemplos de uso para cuando es estático o cuando es dinámico
# Antes de usar esta configuración tienes que revisarla cuidadosamente

server {
    ##################
    # General config #
    ##################

    index index.html index.htm index.nginx-debian.html;
    # Domain name
    server_name plantilla.dinamo.mx www.dinamo.mx;
    # Files root
    root       /var/www/plantilla.dinamo.mx/source;
    access_log /var/log/nginx/plantilla.dinamo.mx.log;
    error_log /var/log/nginx/plantilla.dinamo.mx.error.log;
    autoindex off;

    # Rediección de www a no www
    if ($host = www.plantilla.dinamo.mx) {
        return 301 $scheme://plantilla.dinamo.mx$request_uri;
    }

    ###################################
    # Reverse proxy setup for node.js #
    ###################################

    location / {
        # Snippet or granular, you choose
        include snippets/proxy.conf;
        # Cache Setup (Optional)
        proxy_cache            STATIC;
        proxy_cache_use_stale  error timeout invalid_header updating
                                http_500 http_502 http_503 http_504;
        # Especifica el puerto
        proxy_pass             http://127.0.0.1:3000;
    }

    # Nuxt static serving
    location ~ ^/(images|javascript|js|css|flash|media|static)/ {
        root    /var/www/plantilla.dinamo.mx/source/static;
    }

    location ~ ^/_nuxt/(javascript|js|css)/ {
        root /var/www/plantilla.dinamo.mx/source/.nuxt/dist;
        access_log   off;
    }

    # Allow certbot to work
    location ~ /.well-known {
            root /var/www/plantilla.dinamo.mx/source/static;
    }

    ####################################
    # Static files setup (php or html) #
    ####################################

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
        # Usando laravel
        # try_files $uri $uri/ /index.php?$query_string;
        # Usando Nuxt en modo SPA
        # try_files $uri $uri/ /200.html?$query_string;
    }

    # PHP Setup
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.1-fpm.sock;
    }

    # Para uso estático con nuxt
    # error_page 404 /200.html;
    # location = /200.html {
    #     root /var/www/plantilla.dinamo.mx/html;
    #     internal;
    # }

}