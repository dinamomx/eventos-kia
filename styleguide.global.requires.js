import Vue from 'vue'
import buefy from 'buefy'
import NuxtLink from 'nuxt/lib/app/components/nuxt-link'

Vue.use(buefy)
Vue.component('nuxt-link', NuxtLink)
