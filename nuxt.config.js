/* eslint valid-jsdoc: ["error", { "requireReturn": false }] */
/* eslint-disable import/no-extraneous-dependencies */
import PurgecssPlugin from 'purgecss-webpack-plugin'
import glob from 'glob-all'
import path from 'path'

// Cabezera
import schema from './utils/schema'

const meta = [
  {
    name: 'keywords',
    content: 'Diseño, Contenido, Dinamo',
  },
  { name: 'google-site-verification', content: '' },
]

const link = [
  {
    href: 'https://fonts.googleapis.com/css?family=Quicksand|Rubik:400,700',
    rel: 'stylesheet',
  },
  {
    type: 'text/plain',
    rel: 'author',
    href: 'https://infiniti.dinamo.mx/humans.txt',
  },
]

export default {
  /*
  ** Headers of the page
  */
  head: {
    meta,
    link,
    titleTemplate: (titleChunk = '') =>
      titleChunk ? `${titleChunk} - Planilla Dinamo` : 'Planilla Dinamo',
    script: [
      { innerHTML: JSON.stringify(schema), type: 'application/ld+json' },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    // middleware: ['meta'],
  },
  /**
   * Transición por defecto
   */
  transition: 'page',
  /**
   * CSS global
   */
  css: [
    '~assets/sass/transitions.scss',
    '~assets/sass/app.scss',
    '~assets/sass/global.scss',
    // NOTE: Aquí puedes modificar el estilo de fontawesome
    '@fortawesome/fontawesome-pro/css/fontawesome.css',
    // '@fortawesome/fontawesome-pro/css/light.css',
    '@fortawesome/fontawesome-pro/css/regular.css',
    // '@fortawesome/fontawesome-pro/css/solid.css',
    '@fortawesome/fontawesome-pro/css/brands.css',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~plugins/buefy.js',
    { src: '~plugins/scroll-track.js', nossr: true },
  ],
  /*
  ** Build configuration
  */
  build: {
    babel: {
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        '@babel/syntax-dynamic-import',
      ],
    },
    // Hace el css cacheable
    extractCSS: true,
    // Inserta base/basic en todos lados
    styleResources: {
      scss: './assets/sass/base/_basic.scss',
      sass: './assets/sass/base/_basic.scss',
    },
    /**
     * @param {Object} config The Build Config
     * @param {Object} ctx Nuxt Context
     */
    extend(config, { isDev, isClient }) {
      config.module.rules.push({
        resourceQuery: /blockType=docs/,
        loader: require.resolve('./utils/docs-loader.js'),
      })
      if (!isDev) {
        // Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
        // for more information about purgecss.
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './pages/**/*.vue'),
              path.join(__dirname, './layouts/**/*.vue'),
              path.join(__dirname, './components/**/*.vue'),
            ]),
            whitelist: [
              'html',
              'body',
              'input',
              'help',
              'help.is-danger',
              'input.is-danger',
              'fal',
              'fa-exclamation-circle',
              'fa-lg',
              'has-text-danger',
              'has-icons-right',
            ],
            // whitelistPatterns: [/loading-overlay/],
            whitelistPatternsChildren: [
              /loading-overlay$/,
              /fa-exclamation-circle$/,
              /has-icons-right$/,
            ],
            // keyframes: ['spinAround'],
          })
        )
      }
      // Run ESLINT on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules(?!\/buefy)$/,
        })
      }
    },
  },

  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules: [
    /**
     * Google Analytics, se necesita un UA válido, se puede configurar para
     * envío a diferentes cuentas o vistas de acuerdo al entorno.
     */
    // [
    //   '@nuxtjs/google-analytics',
    //   {
    //     // Puedes usar dos analíticas
    //     id: process.env.PRODUCTION ? 'UA-143980-7' : 'UA-143980-7',
    //     autoTracking: {
    //       exception: true,
    //       page: true,
    //     },
    //     debug: {
    //       enabled: process.env.NODE_ENV !== 'production',
    //       sendHitTask: process.env.NODE_ENV === 'production',
    //     },
    //   },
    // ],
    /**
     * PWA Este módulo integra todo lo necesario para implementar las
     * capacidades PWA a una página web, require ssl
     */
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          generateSW: true,
          // importScripts: ['/notifications-worker.js'],
          InjectManifest: true,
        },
      },
    ],
  ],
  /**
   * Configuraciones que generan automáticamente manifest y
   * etiquetas básicas para seo
   */
  manifest: {
    name: 'Plantilla de dinamo',
    short_name: 'DinamoNuxt',
  },
  meta: {
    name: 'Plantilla de dinamo',
    author: 'Dinamo',
    description: 'Plantilla para ptoyectos realizados por dínamo',
    theme_color: '#f83f28',
    lang: 'es',
    nativeUI: false,
    mobileApp: true,
    appleStatusBarStyle: 'default',
    ogSiteName: true,
    ogTitle: true,
    ogDescription: true,
    ogImage: true,
    ogHost: 'https://plantilla.dinamo.mx',
    ogUrl: true,
    twitterCard: true,
    twitterSite: 'https://plantilla.dinamo.mx',
    twitterCreator: '@dinamomx',
  },
}
