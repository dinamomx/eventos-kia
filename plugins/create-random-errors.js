/**
 * Crea errores aleatorios en el navegador
 * Para confirmar el uso de los anuncios de error
 */

if (process.browser) {
  let n = Math.floor(Math.random() * 100 + 1)
  window.onNuxtReady(() => {
    n = Math.floor(Math.random() * 100 + 1)
    if (n % 2) {
      throw new Error('Random error en nuxtready')
    }
  })
  setTimeout(() => {
    n = Math.floor(Math.random() * 100 + 1)
    if (n % 2) {
      throw new Error('Random error en timeout')
    }
  }, 1000)
  if (n % 2) {
    throw new Error('Random error inmediato')
  }
}
