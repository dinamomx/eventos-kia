module.exports = {
  plugins: {
    // Solo habilitar si se tiene que soportar ie11
    // 'postcss-object-fit-images': {},
    // SEE: https://preset-env.cssdb.org/
    'postcss-preset-env': {
      stage: 1,
      // features: {
      //   // Elimina las advertencias y mantiene las reglas
      //   customProperties: {
      //     preserve: true,
      //     warnings: false,
      //   },
      // },
      // NOTE: https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie
      autoprefixer: {
        grid: true,
      },
    },
    cssnano: {
      preset: 'default',
    },
  },
}
